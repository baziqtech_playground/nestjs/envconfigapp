import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [ConfigModule.forRoot({ isGlobal: true })], // setting isGlobal=true allows to access the environment variables throughout the app ina ny file
  controllers: [AppController], // To access environment variables; process.env.DATABASE_USER
  providers: [AppService],
})
export class AppModule {}
